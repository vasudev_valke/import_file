require 'csv'
require 'nokogiri'
require 'open-uri'
class Detail < ApplicationRecord

def self.to_csv
	detail_attributes = %w{place latitude longitude}
CSV.generate(headers: true) do |csv|
	csv << detail_attributes
	all.each do |detail|
		#data = []
		#data = data.push(post.user.email.split('@')[0])
		csv << detail.attributes.values_at(*detail_attributes)
	end
end
end

def self.import(file)
	CSV.foreach(file.path, headers: true) do |row|
		detail_hash = row.to_hash
		detail = Detail.find_by_place(row["place"])
		if detail == nil
			Detail.create! row.to_hash
		else
			detail.update_attributes(place: detail_hash['place'], latitude: detail_hash['latitude'], longitude: detail_hash['longitude'])
		end
	end
end


end
