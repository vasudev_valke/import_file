class DetailsController < ApplicationController
  def index
  	@details = Detail.all
    respond_to do |format|
    format.html
    format.csv { send_data @details.to_csv }
    end
  end

  def import
  	Detail.import(params[:file])
  	flash.now[:info] = "Successfully imported"
  	redirect_to details_path
  end


  def new
  	@detail = Detail.new
  end

  def create
    @detail = Detail.new(detail_params)
      if @detail.save
       	redirect_to details_path
      else
      	render :new
      end
  end

  def edit
  end

  def update
  end

  def destroy
  	@detail = Detail.find(params[:id])
  @detail_id = @detail.id
    @detail.destroy
    respond_to do |format|
    format.js    
  end
  end

	private
		def detail_params
      		params.require(:detail).permit(:place, :latitude, :longitude)
    	end
end
