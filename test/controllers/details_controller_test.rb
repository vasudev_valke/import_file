require 'test_helper'

class DetailsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get details_index_url
    assert_response :success
  end

  test "should get edit" do
    get details_edit_url
    assert_response :success
  end

  test "should get update" do
    get details_update_url
    assert_response :success
  end

  test "should get delete" do
    get details_delete_url
    assert_response :success
  end

end
